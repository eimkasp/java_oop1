public class Student extends Person {

    private String studentClass;
    private int[] grades = new int[100];

    private double gradesAverage;
    private int gradesCount = 0;

    Student(String name, String surname) {
        super(name, surname);
        this.studentClass = studentClass;
    }

    public double getGradesAverage() {
        return this.gradesAverage;
    }

    private double countAverage() {
        double gradesSum = 0;
        for (int i = 0; i < this.gradesCount; i++) {
            gradesSum += grades[i];
        }

        this.gradesAverage = gradesSum / this.gradesCount;

        return gradesSum / this.gradesCount;
    }

    public void addGrade(int grade) {
        this.grades[this.gradesCount] = grade;
        this.gradesCount++;
        this.countAverage();
    }
}
