public class Person {



    public String name;
    private String surname;
    private int age;
    private String phone;

    public int  getAge() {
        return this.age;
    }


    Person() {

    }

    Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
        System.out.println("Created new Person object: " + this.name);
    }

    Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String fullName() {
        return this.name + " " + this.surname;
    }
}
